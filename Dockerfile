FROM node:12

# Create a directiory for the application
WORKDIR /usr/src/app

# Install application dependencies
COPY package*.json ./

RUN npm install
RUN npm install express jest supertest

# Bundle appplication source files
COPY . .

# Expose port 80 for the node.js server
EXPOSE 80

# Run the server test
RUN npm test

# Run the node.js server
CMD npm run start