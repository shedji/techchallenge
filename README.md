# README #

### IBL Tech challenge ###

A simple API that runs within a Docker image. Returns the git hash, app name and app version to a /health endpoint in json format.

* Version: 1.0.0

JSON structure:

- APIHealth

	- gitHash

	- appName

	- appVersion



## Requirements ##
* Git [https://git-scm.com/downloads]

* Docker [https://www.docker.com/get-started]


### Set up ###

**This will assume that you already have working installations of Git and Docker.**

* Open Command Prompt (Windows) or a Shell Terminal (Linux).

Create a directory that will contain your application.

    mkdir <directoryname>

    cd <directoryname>

Clone the repository from bitbucket

    git clone https://bitbucket.org/shedji/techchallenge.git

    cd techchallenge

**WINDOWS**

Build the docker image (must contain the full stop at the end).

    docker build -t techchallenge .

Run the docker image

    docker run -p 80:80 -d techchallenge

* This will run the application using port 80, the default http port. Can be changed to another port if desired.
* Syntax: docker run -p <port>:80 -d techchallenge

**LINUX**

Build the docker image (must contain the full stop at the end).

    sudo docker build -t techchallenge .

Run the docker image

    sudo docker run -p 80:80 -d techchallenge

* This will run the application using port 80, the default http port. Can be changed to another port if desired.

* Syntax: sudo docker run -p <port>:80 -d techchallenge

### Testing the application is working ###

* Open a web browser and navigate to http://<server ip address>:[port]/health

* Example: http://127.0.0.1/health (default ports), http://127.0.0.1:8080/health (if different port such as 8080 is used).

* Or use **curl http://<your ip address>:[port]/health**

* Example **curl http://127.0.0.1/health**

### Stopping the Docker image ###

Find the Docker image name

    docker ps

* The image name will be under the name column

Stop the Docker image

    docker stop <image_name>


### Information ###

* Creator: Steven Hedji