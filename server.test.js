/* techchallenge server.test.js
* Tests the basic functions of the server.js api using Jest and Supertest
* By Steven Hedji
*/

// Initialize the supertest object
const request = require('supertest');

describe('Testing API functionality', function () {
	
	// Initialize the empty server object
	var server;
	
	// Pre test set up function
	beforeAll(function () {
		// Start the node.js express server
		server = require('./server');
	});
	
	// Post test tear down function
	afterAll(function () {
		// Close the node.js express server
		server.close();
	});

	// Test to see if the /health endpoint returns a 200 ok status
	it('Returns 200 on /health endpoint', async done => {
		const response = await request(server).get('/health');
		// Server response must be 200
		expect(response.status).toBe(200);
		done();
	})
	
	// Test to see if other endpoints return a 404 not found status
	it('Returns 404 on other endpoints', async done => {
		const response = await request(server).get('/test');
		// Server response must be 404
		expect(response.status).toBe(404);
		done();
	})
	
	// Test to see if the header returned from the /health endpoint is json
	it('Returns header as application/json', async done => {
		const response = await request(server).get('/health');
		// Server response header must be application/json
		expect(response.type).toBe('application/json');
		done();
	})
	
	// Test to see if the data returned from the /health endpoint can be parsed as json
	it('Returns json that can be parsed successfully', async done => {
		const response = await request(server).get('/health');		
		const parseJson = () => {
			const jsonRes = response.text;
			var json = JSON.parse(jsonRes);		
		};
		// Must not throw an error when parsing json
		expect(parseJson).not.toThrow();
		done();
	})
	
	// Test to see if the json returned from the /health endpoint contains the correct attributes
	it('Returns json that has correct attributes', async done => {
		const response = await request(server).get('/health');		
		const jsonRes = response.text;
		var json = JSON.parse(jsonRes);
		// Must not have any of the attributes as undefined
		expect(json.APIHealth[0].gitHash).not.toBe(undefined);
		expect(json.APIHealth[0].appName).not.toBe(undefined);	
		expect(json.APIHealth[0].appVersion).not.toBe(undefined);	
		done();	
	})
		
});

