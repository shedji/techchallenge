/* techchallenge server.js
* A simple API that returns the git hash, application name and version to a /health endpoint
* By Steven Hedji
*/

// Initialize variables and objects
var express = require('express'); // Express object
var app = express();
var healthStat = {}; // Array to store app health information
var key = "APIHealth";
var serverPort = 80; // Port for the server to listen on

// Retrieve the git hash value
try{
	
	var gitHashVal = require('child_process').execSync('git rev-parse HEAD').toString().trim()
	
}
catch(err){
	
	console.log("Error retrieving Git hash:");
	console.log(err.message);
	
}

// Create empty array to store health information
healthStat[key] = [];

// Structure the health information in JSON format
var data = {
	
	gitHash: gitHashVal,
	appName: process.env.npm_package_name,
	appVersion: process.env.npm_package_version
	
};

// Push the health information to the healthStat array
healthStat[key].push(data);

// On get /health request
app.get('/health', function (req, res) {
  	
	// Send the health information JSON as a response	
	res.json(healthStat);

})

// Start a server listening on selected port
try{
	
	var server = app.listen(serverPort, "0.0.0.0", function () {
   
		var host = server.address().address;
		var port = server.address().port;

	})
	
}
catch(err){
	console.log("Error starting server:");
	console.log(err.message);
}

// Export the server module for testing
module.exports = server;

